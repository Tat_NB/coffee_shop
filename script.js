'use strict'

let burger = document.querySelector('#burger')
console.log('burger', burger);

let navigation = document.querySelector('#nav')
console.log('nav', nav);

burger.addEventListener('click', () => {
  burger.classList.toggle('header__burger_active')
});

navigation.addEventListener('click', () => {
  navigation.classList.toggle('navigation__active')
});

let button = document.querySelector('#submit')
console.log('button', button);

button.addEventListener('click', (event) => {
  event.preventDefault()
});

let successfulSend = document.querySelector('#success')
console.log('successful', successfulSend);

let error = document.querySelector('#error')
console.log('error', error);
error.classList.remove('form__fields-error_active')

let activeField = document.querySelector('form__fields-error_active')

let inputName = document.querySelector('#name')
console.log('inputName', inputName);

let inputEmail = document.querySelector('#email')
console.log('inputEmail', inputEmail);

let textarea = document.querySelector('#message')
console.log('textarea', textarea);

button.addEventListener('click', () => {
 
  if (inputName.value == '' || inputEmail.value == '' || textarea.value =='') {
    error.classList.add ('form__fields-error_active')
  } else {
    error.classList.remove ('form__fields-error_active')
    successfulSend.classList.add('wrapper-success_active')
    setTimeout(function () {
    successfulSend.classList.remove('wrapper-success_active')
    inputName.value = ''
    inputEmail.value = ''
    textarea.value = ''
    }, 3000)
  }
});
